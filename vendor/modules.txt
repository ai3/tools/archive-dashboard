# git.autistici.org/ai3/go-common v0.0.0-20220929094732-7f570c2aaa30
## explicit
git.autistici.org/ai3/go-common/ldap
# github.com/Azure/go-ntlmssp v0.0.0-20220621081337-cb9428e4ac1e
github.com/Azure/go-ntlmssp
# github.com/beorn7/perks v1.0.1
github.com/beorn7/perks/quantile
# github.com/cenkalti/backoff/v4 v4.1.3
github.com/cenkalti/backoff/v4
# github.com/cespare/xxhash/v2 v2.1.2
github.com/cespare/xxhash/v2
# github.com/go-asn1-ber/asn1-ber v1.5.4
github.com/go-asn1-ber/asn1-ber
# github.com/go-ldap/ldap/v3 v3.4.4
## explicit
github.com/go-ldap/ldap/v3
# github.com/go-logr/logr v1.2.3
github.com/go-logr/logr
github.com/go-logr/logr/funcr
# github.com/go-logr/stdr v1.2.2
github.com/go-logr/stdr
# github.com/golang/protobuf v1.5.2
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
github.com/golang/protobuf/ptypes/timestamp
# github.com/matttproud/golang_protobuf_extensions v1.0.1
github.com/matttproud/golang_protobuf_extensions/pbutil
# github.com/prometheus/client_golang v1.12.2
github.com/prometheus/client_golang/prometheus
github.com/prometheus/client_golang/prometheus/internal
# github.com/prometheus/client_model v0.2.0
github.com/prometheus/client_model/go
# github.com/prometheus/common v0.32.1
github.com/prometheus/common/expfmt
github.com/prometheus/common/internal/bitbucket.org/ww/goautoneg
github.com/prometheus/common/model
# github.com/prometheus/procfs v0.7.3
github.com/prometheus/procfs
github.com/prometheus/procfs/internal/fs
github.com/prometheus/procfs/internal/util
# go.opentelemetry.io/otel v1.10.0
go.opentelemetry.io/otel
go.opentelemetry.io/otel/attribute
go.opentelemetry.io/otel/baggage
go.opentelemetry.io/otel/codes
go.opentelemetry.io/otel/internal
go.opentelemetry.io/otel/internal/baggage
go.opentelemetry.io/otel/internal/global
go.opentelemetry.io/otel/propagation
# go.opentelemetry.io/otel/trace v1.10.0
go.opentelemetry.io/otel/trace
# golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
golang.org/x/crypto/md4
# golang.org/x/sys v0.0.0-20220114195835-da31bd327af9
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# google.golang.org/protobuf v1.27.1
google.golang.org/protobuf/encoding/prototext
google.golang.org/protobuf/encoding/protowire
google.golang.org/protobuf/internal/descfmt
google.golang.org/protobuf/internal/descopts
google.golang.org/protobuf/internal/detrand
google.golang.org/protobuf/internal/encoding/defval
google.golang.org/protobuf/internal/encoding/messageset
google.golang.org/protobuf/internal/encoding/tag
google.golang.org/protobuf/internal/encoding/text
google.golang.org/protobuf/internal/errors
google.golang.org/protobuf/internal/filedesc
google.golang.org/protobuf/internal/filetype
google.golang.org/protobuf/internal/flags
google.golang.org/protobuf/internal/genid
google.golang.org/protobuf/internal/impl
google.golang.org/protobuf/internal/order
google.golang.org/protobuf/internal/pragma
google.golang.org/protobuf/internal/set
google.golang.org/protobuf/internal/strs
google.golang.org/protobuf/internal/version
google.golang.org/protobuf/proto
google.golang.org/protobuf/reflect/protodesc
google.golang.org/protobuf/reflect/protoreflect
google.golang.org/protobuf/reflect/protoregistry
google.golang.org/protobuf/runtime/protoiface
google.golang.org/protobuf/runtime/protoimpl
google.golang.org/protobuf/types/descriptorpb
google.golang.org/protobuf/types/known/anypb
google.golang.org/protobuf/types/known/durationpb
google.golang.org/protobuf/types/known/timestamppb
