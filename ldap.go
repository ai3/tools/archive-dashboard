package main

import (
	"context"
	"fmt"
	"log"
	"sort"
	"sync"
	"time"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	"github.com/go-ldap/ldap/v3"
)

var (
	defaultLDAPURI = "ldapi:///var/run/ldap/ldapi"
	baseDN         = "ou=People,dc=investici,dc=org,o=Anarchy"
	searchFilter   = "(&(|(objectClass=virtualHost)(objectClass=subSite))(status=archived))"
	searchAttrs    = []string{"alias", "cn", "parentSite"}

	updateInterval = 60 * time.Minute
	updateTimeout  = 300 * time.Second
)

func getURLsFromLDAP(ctx context.Context, pool *ldaputil.ConnectionPool) ([]string, error) {
	req := ldap.NewSearchRequest(
		baseDN,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		searchFilter,
		searchAttrs,
		nil,
	)
	result, err := pool.Search(ctx, req)
	if err != nil {
		return nil, err
	}

	var urls []string
	for _, entry := range result.Entries {
		urls = append(urls, urlFromEntry(entry))
	}
	return urls, nil
}

func urlFromEntry(entry *ldap.Entry) string {
	if cn := entry.GetAttributeValue("cn"); cn != "" {
		return cn
	}
	return fmt.Sprintf(
		"%s/%s",
		entry.GetAttributeValue("parentSite"),
		entry.GetAttributeValue("alias"),
	)
}

// Store the list of archived websites, and keep it fresh by updating
// it periodically from LDAP. The list doesn't change too often, so a
// 1h update interval seems reasonable.
type archivedURLList struct {
	pool *ldaputil.ConnectionPool

	mx   sync.Mutex
	urls []string
}

func newArchivedURLList(pool *ldaputil.ConnectionPool) *archivedURLList {
	return &archivedURLList{pool: pool}
}

func (l *archivedURLList) getURLs() []string {
	l.mx.Lock()
	defer l.mx.Unlock()
	return l.urls
}

func (l *archivedURLList) update(ctx context.Context) error {
	urls, err := getURLsFromLDAP(ctx, l.pool)
	if err != nil {
		return err
	}

	sort.Strings(urls)

	l.mx.Lock()
	l.urls = urls
	l.mx.Unlock()
	return nil
}

func (l *archivedURLList) loop(ctx context.Context) {
	t := time.NewTicker(updateInterval)
	defer t.Stop()
	for {
		uctx, cancel := context.WithTimeout(ctx, updateTimeout)
		err := l.update(uctx)
		cancel()
		if err != nil {
			log.Printf("update error: %v", err)
		}

		select {
		case <-t.C:
		case <-ctx.Done():
			return
		}
	}
}
