package main

import (
	"context"
	"flag"
	"html/template"
	"log"
	"net/http"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
)

var (
	archivedURLsTplSrc = `<doctype !html>
<html lang="en">
 <head>
  <title>archive.autistici.org</title>
  <meta charset="utf-8">
  <meta name="lang" content="en">
  <meta name="author" content="Autistici/Inventati">
  <meta name="description" content="Website archive of autistici.org">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style type="text/css">
body {
  background: white;
  font-family: sans-serif; 
  width: 90%;
  margin: 30px auto;
}
.title {
  background:
     url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAgCAMAAACvkzHFAAAC2VBMVEUAACH//////Pz/+fr/+fn+z9H9mp/9jZL+xMb/9vf//v7+tLj8Pkf8DBj8BxP8MTv9lZr+8PH///7+///+5OX9Vl78BRH8Chb8Cxf8BRL8Iy7+ur7+//78N0D8CBT9ipD/9/j+0dP8O0X8CRX8ERz9oqb//f7+5Ob8Hyr+vcD//f3+/v7+9vf9dHv8BhP8Qkz+1tj+/f39iI38BhL9aXH/8vL+8/P+o6j9VV79XWX+tbj+8vP+oKX8FiL8Chf9jJH+1Nb+wcT+5eb/9fX9fYT8GiX8Gyb9WWH/8/T+ur38KTT8ICv+rbH/+/z+6er+sbT9S1T8PEb+sLT/+vv+uLv8KTP8DRn8KDL9eH/+zc//9vb+2Nr8QEr8Q0z+2tz+3+H9pan9VFz8GCP8ND7+wMP9j5T8FB/9io/+2dv86Or7TFT8CBX9XGT+/Pz+29z9nqL9Tlb8GSX8Dhr9gon9iI78ER38Dxv9R1D7bnX7JjD8Ljn9SlP8FCD9bXT/7/D+r7L8JC/8BxT9dXv9hIn8Iiz8EBv8JzL9rLD+9/j9vL/9hIr8Ymn8Q038MDr8HSj8Ex/8DRj8Eh78FiH8HCf8JTD9Tlf+pKj+8vL+4eL+0tT9pqv9hYv9ZWz8KDP9aXD9goj9jpP9pKj9rrL+wsX+y83+1df+5+j+6+z9YWj9YGj+9fX9k5n8JS/+mp//+Pj8Hin7ER37DBj8ISz+mZ7+4OH9Z278FSH8NT/8nqL7PUb9kpf+8/T/+/v+ztD9VV39kJX9mZ78Eh38HCj9i5D+xsn9RU79Qkv/3t/8NkD+19n8CRT+n6P/+vr+9fb9naL+rLD9WGD8SVL+3d/+o6f+p6v++Pj9eH78LDb+yMv+u7/9X2f/7+/8LDf8P0j/8PH8FyP8PUb9cXf/9PX+7e3+m6D8RU7+v8L+2dr9YWn8ISv8Mz39i5H+7e7/5eb+2tv+4+X9wMP+z9L+9PX46U/rAAAAAXRSTlMAQObYZgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfjCAkIAyAPLOV0AAACA0lEQVQ4y2NgIB2wsrGzceBXws3Dy8fLL4BPiYiomLiEuKSUND6bZOXEgYBXHo8aRSWQGmUVVTxq1EX5JIBqNDTxOUhXD6iGT98AnxojYwlxCWMTLDLmFpZWEJaNLdA9dvZQcQdHJ5gSF1dxXjd3RxDT00tZXNnbB8T08w+Q0ggMgigJCQV6RiIsPCKSgSE6RllCOTaOgSE+ITGJl5dXOTkFrCY1TRlogYSEhGx6BkNWtkR2Tm5efkEhUAToxaJisJqSUpAaIJArK6+oBJpTFVFdIwcREuerrQOpqW+ACSjLyYGUKzc2QnUB2U1gc+qaW1rDxJWBQBwBQFxluWzltnaoxzq7unt6G/r6JWAGyk2YOGmyxpTw3qnTkIJoxkzHWbPnhELVzJ03f8HCRYuXLF2GEZbLV0CsU165HGdMrF4DVTNlLQ4Vy+evB4XTho1ARZs2Y1WyZes2XqCa7Tt2gozS2IVFyd59ZSC5sv0MBw6CrDt0GEPJkaMgFRJyx44zZJwAuUrODT3Bnqw+BXZt/2lQIlKWACs6h6rmfCtIWCL7AijBRCaDvcd38RKKmsu8IPcq910B866BDVK+fgNFzc0+UDRI3LoN5jneAfH47qIl6/n3jfXkHsA0PuSVUzaelIrm6MdPRJ+ueAbjPX/xcv2r128wfP/2nR+C8/6D50eSyhIAzOiRAP3JGUkAAAAASUVORK5CYII=)
     no-repeat
     left center;
  padding-left: 50px;
}
a {
  text-decoration: none;
}
  </style>
 </head>
 <body>

  <h1 class="title">archive.autistici.org</h1>

  <p>
    List of archived websites:
  </p>

  <ul>
  {{range $url := .URLs}}
    <li>
      <a href="https://archive.autistici.org/ai/*/https://{{$url}}/">{{$url}}</a>
    </li>
  {{end}}
  </ul>

 </body>
</html>`
	archivedURLsTpl = template.Must(template.New("").Parse(archivedURLsTplSrc))

	addr       = flag.String("addr", ":2073", "address to listen on")
	ldapURI    = flag.String("ldap-uri", defaultLDAPURI, "LDAP URI to connect to")
	ldapBindDN = flag.String("ldap-bind-dn", "", "LDAP bind DN")
	ldapBindPW = flag.String("ldap-bind-password", "", "LDAP bind password")
)

func main() {
	log.SetFlags(0)
	flag.Parse()

	pool, err := ldaputil.NewConnectionPool(*ldapURI, *ldapBindDN, *ldapBindPW, 2)
	if err != nil {
		log.Fatalf("LDAP connection error: %v", err)
	}
	urlList := newArchivedURLList(pool)
	go urlList.loop(context.Background())

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path != "/" {
			http.NotFound(w, req)
			return
		}

		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		w.Header().Set("Cache-Control", "no-store")
		if err := archivedURLsTpl.Execute(w, map[string]interface{}{
			"URLs": urlList.getURLs(),
		}); err != nil {
			log.Printf("template error: %v", err)
		}
	})

	err = http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Printf("error: %v", err)
	}
}
