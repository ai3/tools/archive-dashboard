FROM golang:1.18 AS build

ADD . /src
WORKDIR /src
RUN go build -ldflags='-extldflags=-static -w -s' -tags osusergo,netgo .

FROM scratch

COPY --from=build /src/archive-dashboard /archive-dashboard

ENTRYPOINT ["/archive-dashboard"]

