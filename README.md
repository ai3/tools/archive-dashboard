archive-dashboard
===

Simple landing page for archive.autistici.org, listing all the
publicly archived websites and providing direct links for them to our
Wayback Machine.

The server just renders a simple HTML template with the list of
archived websites, which is obtained from LDAP by querying for
*status=archived* websites. The list is cached and updated
periodically.
