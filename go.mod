module git.autistici.org/ai3/tools/archive-dashboard

go 1.15

require (
	git.autistici.org/ai3/go-common v0.0.0-20220929094732-7f570c2aaa30
	github.com/go-ldap/ldap/v3 v3.4.4
)
